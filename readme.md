# API NODEJS

## Installation des dépendances npm

Installe les dépendances du projet avec [npm](https://nodejs.org/en/download/)

```bash
npm install
```

## Démarrage du serveur de développement

Avant de démarrer le serveur de développement assure toi d'attribuer des valeurs pour les variables présentes dans le fichier .en.development

Pour démarrer le serveur de développement avec [npm](https://nodejs.org/en/download/)

```bash
npm run dev
```