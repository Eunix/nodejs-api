import { config } from "dotenv";
import path from "path";

config({ path: path.resolve(__dirname, `../../.env.${process.env.NODE_ENV}`) })

const Env = {
    env: String(process.env.NODE_ENV),
    key: String(process.env.APP_KEY),
    host: String(process.env.DATABASE_HOST),
    port: Number(process.env.DATABASE_PORT),
    username: String(process.env.DATABASE_USER),
    password: String(process.env.DATABASE_PASSWORD),
    database: String(process.env.DATABASE_NAME),
}

export default Env;