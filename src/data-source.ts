import Env from "./configs/config"
import "reflect-metadata"
import { DataSource } from "typeorm"

export const ApplicationDataSource = new DataSource({
    type: "mysql",
    host: Env.host,
    port: Env.port,
    username: Env.username,
    password: Env.password,
    database: Env.database,
    synchronize: Env.env === 'development',
    logging: Env.env === 'development',
    entities: [Env.env === "production" ? "./build/entities/**/*.js" : "src/entities/**/*.ts"],
    migrations: [],
    subscribers: [],
})