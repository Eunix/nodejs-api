import {User} from "../entities/user.entity";
import { ApplicationDataSource } from "../data-source";
import { Repository } from "typeorm";
import UpdateUserDto from "../dtos/user/update.dto";

export class UserService {

    private readonly repository: Repository<User>

    constructor() {
        this.repository = ApplicationDataSource.getRepository(User)
    }

    public async loadUser(authenticatedUser: any) {
        return await this.repository.findOneBy({
            id: authenticatedUser.id
        })
    }

    public async updateUser(authenticatedUser: any, updateData: UpdateUserDto) {
        return await this.repository.update(authenticatedUser.id, updateData)
    }

}