import * as jwt from "jsonwebtoken"
import * as bcrypt from "bcrypt"
import {ApplicationDataSource} from "../data-source";
import {User} from "../entities/user.entity";
import { Repository } from "typeorm";
import CreateUserDto from "../dtos/user/create.dto";
import LoginDto from "../dtos/user/login.dto";
import PasswordDto from "../dtos/user/password.dto";
import {Profile} from "../entities/profile.entity";
import Env from "../configs/config";

export default class AuthenticationService {

    private readonly userRepository: Repository<User>
    private readonly profileRepository: Repository<Profile>

    constructor() {
        this.userRepository = ApplicationDataSource.getRepository(User)
        this.profileRepository = ApplicationDataSource.getRepository(Profile)
    }

    public async registration(data: CreateUserDto) {
        const bcryptPassword = AuthenticationService.cryptPassword(data.password)

        return this.userRepository.create({
            password: bcryptPassword ,
            ... data
        });
    }

    public async login(data: LoginDto) {
        const user = await this.userRepository.findOneBy({
            email: data.email
        })

        if (user) {
            if (AuthenticationService.comparePassword(data.password, user.password)) {
                const authenticatedUser = { id: user.id, email: user.email, firstName: user.firstName, lastName: user.lastName }

                return jwt.sign(authenticatedUser , Env.key , {
                    algorithm: "HS512" ,
                    expiresIn: "1h"
                });
            } else {
                throw Error("Password does not match with record!")
            }
        } else {
            throw Error(`Unknown  user with email: ${data.email}`)
        }
    }

    public async updatePassword(authenticatedUser: any, data: PasswordDto) {
        const user = await this.userRepository.findOneBy({
            id: authenticatedUser.id
        })

        if (user) {
            if (AuthenticationService.comparePassword(data.password, user.password) ) {
                user.password = AuthenticationService.cryptPassword(data.newPassword)
                return this.userRepository.save(user);
            } else {
                throw new Error("Old password not match with our record")
            }
        } else {
            throw new Error("User not found !")
        }
    }

    private static comparePassword(password: string, hashedPassword: string): boolean {
        return bcrypt.compareSync(password, hashedPassword)
    }

    private static cryptPassword(password: string): string {
        return bcrypt.hashSync(password, 12)
    }

}