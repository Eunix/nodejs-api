export default interface PasswordDto {
    password: string
    newPassword: string
}