export default interface UpdateUserDto {
    email: string
    firstName: string
    lastName: string
}