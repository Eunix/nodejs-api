import AuthenticationService from "../services/authentication.service";
import {NextFunction , Request , Response} from "express"
import AuthenticatedUser from "../interfaces/authenticatedUser";

export default class AuthenticationController {
    private readonly service: AuthenticationService

    constructor() {
        this.service = new AuthenticationService()
    }

    public registrationHandler = async(req: Request, res: Response, next: NextFunction) => {
        try {
            const registration = await this.service.registration(req.body)
            return res.status(201).json(registration)
        }catch (e) {
            next(new Error(e.message))
        }
    }

    public loginRegistrationHandler = async(req: Request, res: Response, next: NextFunction) => {
        try {
            const login = await this.service.login(req.body)
            return res.status(201).json(login)
        }catch (e) {
            next(new Error(e.message))
        }
    }

    public updatePasswordHandler = async(req: AuthenticatedUser, res: Response, next: NextFunction) => {
        try {
            const updatePassword = await this.service.updatePassword(req.user, req.body)
            return res.status(200).json(updatePassword)
        }catch (e) {
            next(new Error(e.message))
        }
    }

}