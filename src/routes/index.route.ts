import {Router} from "express";
import authenticationRoutes from "./authentication.route";
import authenticationMiddleware from "../middlewares/authentication.middleware";
import userRoutes from "./user.route";

const apiRouter = Router()

apiRouter.use('/authentication', authenticationRoutes)
apiRouter.use('/users', authenticationMiddleware, userRoutes)

export default apiRouter