import {Router} from "express";
import AuthenticationController from "../controllers/authentication.controller";
import authenticationMiddleware from "../middlewares/authentication.middleware";

const controller = new AuthenticationController()
const authenticationRoutes = Router()

authenticationRoutes.post('/register', controller.registrationHandler)
authenticationRoutes.post('/login', controller.loginRegistrationHandler)
authenticationRoutes.put('/update-password', authenticationMiddleware, controller.updatePasswordHandler)

export default authenticationRoutes