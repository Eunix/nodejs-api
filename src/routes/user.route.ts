import {Router} from "express";
import UserController from "../controllers/user.controller";

const userRoutes = Router()
const controller = new UserController()

userRoutes.get('/profile', controller.loadUserHandler)
userRoutes.put('/', controller.updateUserHandler)

export default userRoutes