import BaseModel from "./base.entity";
import {Column , Entity , ManyToOne} from "typeorm";
import {User} from "./user.entity";

@Entity("addresses")
export class Address extends BaseModel{
    @ManyToOne(() => User, (user) => user.addresses)
    user: User

    @Column({type: "varchar"})
    country: string

    @Column({type: "varchar"})
    city: string
}