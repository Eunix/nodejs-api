import {Column , Entity , JoinColumn , OneToMany , OneToOne} from "typeorm";
import BaseModel from "./base.entity";
import {Address} from "./address.entity";
import {Profile} from "./profile.entity";


@Entity("users")
export class User extends BaseModel {
    @Column({type: "varchar"})
    firstName: string

    @Column({type: "varchar"})
    lastName: string

    @Column({type: "varchar", unique: true})
    email: string

    @Column({type: "varchar"})
    password: string

    @OneToMany(() => Address, (address) => address.user)
    addresses: Address[]

    @OneToOne(() => Profile)
    @JoinColumn()
    profile: Profile
}