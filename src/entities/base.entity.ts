import {
    BaseEntity ,
    CreateDateColumn ,
    Entity ,
    PrimaryGeneratedColumn ,
    UpdateDateColumn
} from "typeorm"

@Entity()
export default abstract class BaseModel extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number

    @CreateDateColumn()
    public createdDatetime?: Date

    @UpdateDateColumn()
    public updatedDatetime?: Date
}