import BaseModel from "./base.entity";
import {Column , Entity} from "typeorm";

@Entity("profiles")
export class Profile extends BaseModel  {
    @Column({type: "varchar"})
    name: string
}