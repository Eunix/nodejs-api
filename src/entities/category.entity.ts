import BaseModel from "./base.entity";
import {Column , Entity} from "typeorm";

@Entity("categories")
export class Category extends BaseModel{

    @Column({type: "varchar"})
    name: string
}