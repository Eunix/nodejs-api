import BaseModel from "./base.entity";
import {Column , Entity , JoinTable , ManyToMany} from "typeorm";
import {Category} from "./category.entity";

@Entity("products")
export class Product extends BaseModel {
    @Column({type: "varchar"})
    name: string

    @ManyToMany(() => Category)
    @JoinTable()
    categories: Category[]
}