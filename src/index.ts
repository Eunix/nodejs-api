import express , {NextFunction , Request , Response} from "express"
import { ApplicationDataSource } from "./data-source"
import apiRouter from "./routes/index.route";

ApplicationDataSource.initialize().then(async ()=> {
    console.log("Datasource has been initialized")
}).catch((error => {
    console.log(error)
}))

const app = express()

app.use(express.json())

app.use('/api', apiRouter)

app.listen(3000, () => {
    console.log("Node server has started")
})

app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
    return res.status(500).json({
        success: false,
        name: error.name
    })
})